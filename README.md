### LISTA DE CAMBIOS ###

**1.2** - *Enviada para pasar a producción*

	- Eliminamos el resumen global de informes y mostramos en su defecto el resumen a nivel de ruta (petición de clte).
	- Fix: Ahora se cargan el cobrado y efectivo a 0 cuando se abre una factura nula desde informes.

**1.1**

	- Se optimiza el cálculo automático de valores al variar el abono.
	- Fix: Arreglo el envío de facturas con pagos parciales (antes se enviaban siempre como pagos completos).
	- Fix: Ahora se muestra correctamente el recuento de pagos mixtos (informes).

**1.0.1**

	- Documentación del código fuente al completo, excepto layouts XML.
	- Optimización y reutilización del código.


**1.0** - *Producción*

	- Versión inicial.