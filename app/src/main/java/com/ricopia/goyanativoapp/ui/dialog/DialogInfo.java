package com.ricopia.goyanativoapp.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.ricopia.goyanativoapp.R;


/**
 * Diálgo para mostrar un mensaje informativo al usuario
 */
public class DialogInfo extends DialogFragment {

    private String title;
    private String message;

    public DialogInfo() {}

    public void set(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public void set(String message) {
        this.title = null;
        this.message = message;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialog);
        // Diálogo con Título y mensaje
        if(this.title != null)
            builder.setTitle(this.title)
                    .setMessage(this.message)
                    .setPositiveButton("Entendido", null);
        // Diálogo únicamente con mensaje
        else
            builder.setMessage(this.message)
                    .setPositiveButton("Entendido", null);
        return builder.create();
    }
}
