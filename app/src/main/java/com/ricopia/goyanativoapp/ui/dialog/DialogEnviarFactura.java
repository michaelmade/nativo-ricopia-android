package com.ricopia.goyanativoapp.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.ricopia.goyanativoapp.EnviarFacturaActivity;
import com.ricopia.goyanativoapp.R;


public class DialogEnviarFactura extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialog);
        builder.setTitle("Factura: "+arguments.getString("factura"))
                .setMessage("¿Desea enviar la factura?\n\n"+arguments.getString("datos"))
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((EnviarFacturaActivity) getActivity()).enviarFactura(false);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {}
                });
        return builder.create();
    }
}
