package com.ricopia.goyanativoapp.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;
import com.ricopia.goyanativoapp.obj.G;
import com.ricopia.goyanativoapp.R;
import com.ricopia.goyanativoapp.obj.Factura;
import com.ricopia.goyanativoapp.obj.Ruta;
import java.util.ArrayList;


/**
 * Adaptador basado en RutaAdapter para cargar la lista de rutas que devuelve el informe en una lista expansible.
 * El adaptador establece un listener para abrir la factura seleccionada y poder editarla para volver a enviarla.
 */
public class RutaAdapterInforme extends RutaAdapter {

    private InterfaceRutas interfaceRutas;
    private LayoutInflater inflater;

    // Views del resumen global
    private TextView txvTotalEfectivos;
    private TextView txvTotalTalones;
    private TextView txvNumeroTalones;
    private TextView txvFirma;
    private TextView txvMixto;
    private TextView txvParcial;
    private TextView txvNulos;
    private TextView txvPagado;
    private TextView txvCheque;
    private TextView txvParcialMixto;
    private TextView txvParcialCheque;


    public RutaAdapterInforme(Context context, ArrayList<Ruta> rutas, ExpandableListView elv, InterfaceRutas interfaceRutas) {
        super(context, rutas, elv, interfaceRutas);
        this.interfaceRutas = interfaceRutas; // InformeRutasFr, necesario para abrir actividad de envío de facturas.
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); // Inicializamos servicio de carga layout
    }

    // Holder para las FACTURAS
    public class MyHolderFactura {
        public int groupPosition;
        public int childPosition;
        public TextView txvFactura;
        public TextView txvCliente;
        public TextView txvEstatus;
        public TextView txvImporte;
        public TextView txvCompleto;
        public TextView txvAnterior;
        public TextView txvParcial;
        public TextView txvCompletoTalon;
        public TextView txvAnteriorTalon;
        public TextView txvParcialTalon;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
        showResumenRuta(groupPosition);
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
        showResumenRuta(-1);
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, final View convertView, ViewGroup parent) {
        View v = convertView;
        MyHolderFactura holder = new MyHolderFactura();

        if(v == null) { // Si el ítem no es reciclable, cargamos las Views
            if(getChildType(groupPosition, childPosition) == 0) v = inflater.inflate(R.layout.informe_item_factura, parent, false);
            else                                                v = inflater.inflate(R.layout.informe_item_factura_pendiente, parent, false);

            holder.txvFactura = (TextView)v.findViewById(R.id.txvFactura);
            holder.txvCliente = (TextView)v.findViewById(R.id.txvCliente);
            holder.txvEstatus = (TextView)v.findViewById(R.id.txvEstatus);
            holder.txvImporte = (TextView)v.findViewById(R.id.txvImporte);
            holder.txvCompleto = (TextView) v.findViewById(R.id.txvCompleto);
            holder.txvAnterior = (TextView) v.findViewById(R.id.txvAnterior);
            holder.txvParcial = (TextView) v.findViewById(R.id.txvParcial);
            holder.txvCompletoTalon = (TextView) v.findViewById(R.id.txvCompletoTalon);
            holder.txvAnteriorTalon = (TextView) v.findViewById(R.id.txvAnteriorTalon);
            holder.txvParcialTalon = (TextView) v.findViewById(R.id.txvParcialTalon);
            v.setTag(holder);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyHolderFactura holder = (MyHolderFactura) v.getTag();
                    interfaceRutas.launchEnviarFacturaActivity(rutas, holder.groupPosition, holder.childPosition);
                }
            });
        }
        else // Si el ítem se puede reciclar, obtenemos las Views
            holder = (MyHolderFactura) v.getTag();

        holder.childPosition = childPosition;
        holder.groupPosition = groupPosition;
        holder.txvFactura.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).NUMERO);
        holder.txvCliente.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).NOMBRECLIENTE + " (" + rutas.get(groupPosition).FACTURAS.get(childPosition).CODIGOCLIENTE + ")");
        holder.txvEstatus.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).ESTATUS);
        holder.txvImporte.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).IMPORTE + " €");
        showResumenFactura(holder, groupPosition, childPosition); // Mostramos el resumen de la factura

        return v;
    }

    /**
     * Vistas del resumen de ruta
     * @param v Vista padre (resumen View).
     */
    public void setTxvResumen(View v) {
        txvTotalEfectivos = (TextView) v.findViewById(R.id.txvTotalEfectivos);
        txvTotalTalones = (TextView) v.findViewById(R.id.txvTotalTalones);
        txvNumeroTalones = (TextView) v.findViewById(R.id.txvNumeroTalones);
        txvFirma = (TextView) v.findViewById(R.id.txvFirma);
        txvMixto = (TextView) v.findViewById(R.id.txvMixto);
        txvParcial = (TextView) v.findViewById(R.id.txvParcial);
        txvNulos = (TextView) v.findViewById(R.id.txvNulos);
        txvPagado = (TextView) v.findViewById(R.id.txvPagado);
        txvCheque = (TextView) v.findViewById(R.id.txvCheque);
        txvParcialMixto = (TextView) v.findViewById(R.id.txvParcialMixto);
        txvParcialCheque = (TextView) v.findViewById(R.id.txvParcialCheque);
    }


    /**
     * Mustra un breve resumen de contabilidad de una ruta determinada.
     *
     * @param nRuta  Indice de la ruta
     */
    private void showResumenRuta(int nRuta) {
        if(nRuta != -1) {
            float totalEfectivos = 0,  totalTalones = 0;
            int talones = 0;
            int firma=0, mixto=0, parcial=0, nulos=0, pagado=0, cheque=0, parcialMixto=0, parcialCheque=0;

            try {
                Factura f;
                for (int j = 0; j < rutas.get(nRuta).FACTURAS.size(); j++) {
                    f = rutas.get(nRuta).FACTURAS.get(j);
                    totalEfectivos = G.oper(totalEfectivos, G.getFloatFromString(f.TOTALEFECTIVO), true); // +TOTALEFECTIVO
                    totalEfectivos = G.oper(totalEfectivos, G.getFloatFromString(f.PAGOPARCIAL), true); // +PAGOPARCIAL
                    totalEfectivos = G.oper(totalEfectivos, G.getFloatFromString(f.PAGOANTERIOR), true); // +PAGOANTERIOR
                    float talon = G.getFloatFromString(f.TOTALCHEQUE);
                    if(talon > 0) {
                        totalTalones = G.oper(totalTalones, talon, true); // +TOTALCHEQUE
                        talones++;
                    }
                    talon = G.getFloatFromString(f.PAGOPARCIALCHEQUE);
                    if(talon > 0) {
                        totalTalones = G.oper(totalTalones, talon, true); // +PAGOPARCIALCHEQUE
                        talones++;
                    }
                    talon = G.getFloatFromString(f.PAGOANTERIORCHEQUE);
                    if(talon > 0) {
                        totalTalones = G.oper(totalTalones, talon, true); // +PAGOANTERIORCHEQUE
                        talones++;
                    }

                    // Contabilizamos los tipos de factura que tiene la ruta
                    if (f.ESTATUS.equals("FIRMA"))                firma++;
                    else if (f.ESTATUS.equals("MIXTO"))           mixto++;
                    else if (f.ESTATUS.equals("P.PARCIAL"))       parcial++;
                    else if (f.ESTATUS.equals("NULO"))            nulos++;
                    else if (f.ESTATUS.equals("PAGADO"))          pagado++;
                    else if (f.ESTATUS.equals("CHEQUE"))          cheque++;
                    else if (f.ESTATUS.equals("P.PARCIALMIXTO"))  parcialMixto++;
                    else if (f.ESTATUS.equals("P.PARCIALCHEQUE")) parcialCheque++;
                }
            }
            catch (Exception ex) {
                Toast.makeText(context, "EXCEPCIÓN al cargar informe resumen (ruta) " + ex.getMessage() + " // " + ex.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }

            txvTotalEfectivos.setText(totalEfectivos+" €");
            txvTotalTalones.setText(totalTalones+" €");
            txvNumeroTalones.setText(Integer.toString(talones));
            txvFirma.setText(String.valueOf(firma));
            txvMixto.setText(String.valueOf(mixto));
            txvParcial.setText(String.valueOf(parcial));
            txvNulos.setText(String.valueOf(nulos));
            txvPagado.setText(String.valueOf(pagado));
            txvCheque.setText(String.valueOf(cheque));
            txvParcialMixto.setText(String.valueOf(parcialMixto));
            txvParcialCheque.setText(String.valueOf(parcialCheque));
        }
        else {
            txvTotalEfectivos.setText("");
            txvTotalTalones.setText("");
            txvNumeroTalones.setText("");
            txvFirma.setText("");
            txvMixto.setText("");
            txvParcial.setText("");
            txvNulos.setText("");
            txvPagado.setText("");
            txvCheque.setText("");
            txvParcialMixto.setText("");
            txvParcialCheque.setText("");
        }
    }

    /**
     * Mustra un breve resumen de contabilidad de una factura determinada.
     *
     * @param holder   Holder que contiene las Views sobre las que cargaremos el resumen.
     * @param nRuta    Indice de la ruta a la que pertenece la factura.
     * @param nFactura Indice de la factura.
     */
    private void showResumenFactura(MyHolderFactura holder, int nRuta, int nFactura) {
        float completo = 0, anterior = 0, parcial = 0, completoTalon = 0, anteriorTalon = 0, parcialTalon = 0;

        try {
            Factura f = rutas.get(nRuta).FACTURAS.get(nFactura);
            completo = G.oper(completo, G.getFloatFromString(f.TOTALEFECTIVO), true); // +
            anterior = G.oper(anterior, G.getFloatFromString(f.PAGOANTERIOR), true); // +
            parcial = G.oper(parcial, G.getFloatFromString(f.PAGOPARCIAL), true); // +
            completoTalon = G.oper(completoTalon, G.getFloatFromString(f.TOTALCHEQUE), true); // +
            anteriorTalon = G.oper(anteriorTalon, G.getFloatFromString(f.PAGOANTERIORCHEQUE), true); // +
            parcialTalon = G.oper(parcialTalon, G.getFloatFromString(f.PAGOPARCIALCHEQUE), true); // +
        }
        catch (Exception ex) {
            Toast.makeText(context, "EXCEPCIÓN al cargar informe resumen (factura) " + ex.getMessage() + " // " + ex.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }

        holder.txvCompleto.setText(Float.toString(completo)+" €");
        holder.txvAnterior.setText(Float.toString(anterior)+" €");
        holder.txvParcial.setText(Float.toString(parcial)+" €");
        holder.txvCompletoTalon.setText(Float.toString(completoTalon)+" €");
        holder.txvAnteriorTalon.setText(Float.toString(anteriorTalon)+" €");
        holder.txvParcialTalon.setText(Float.toString(parcialTalon)+" €");
    }
}
