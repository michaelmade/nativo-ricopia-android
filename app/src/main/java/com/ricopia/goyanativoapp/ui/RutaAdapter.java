package com.ricopia.goyanativoapp.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.ricopia.goyanativoapp.R;
import com.ricopia.goyanativoapp.obj.Ruta;
import java.util.ArrayList;


/**
 * Adaptador base para cargar la lista de rutas recibidas en una lista expansible.
 * El adaptador establece un listener para abrir la factura seleccionada.
 */
public class RutaAdapter extends BaseExpandableListAdapter {

    public ArrayList<Ruta> rutas;
    public Context context;
    private int expanded; // Grupo expandido actualmente
    private int lastExpanded; // Último grupo expandido
    private ExpandableListView elv;
    private InterfaceRutas interfaceRutas; // CargarRutasFr, necesario para abrir actividad de envío de facturas.
    private LayoutInflater inflater;

    public RutaAdapter(Context context, ArrayList<Ruta> rutas, ExpandableListView elv, InterfaceRutas interfaceRutas) {
        this.rutas = rutas;
        this.context = context;
        this.expanded = lastExpanded = -1; // No hay ningún grupo expandido
        this.elv = elv;
        this.interfaceRutas = interfaceRutas;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); // Inicializamos servicio de carga layout
    }

    // Holder para las RUTAS
    public class MyHolder {
        public TextView txvId;
        public TextView txvFecha;
        public TextView txvSizeFacturas;
    }

    // Holder para las FACTURAS
    public class MyHolderFactura {
        public int groupPosition;
        public int childPosition;
        public TextView txvFactura;
        public TextView txvCliente;
        public TextView txvDireccion;
        public TextView txvCodigoPostal;
        public TextView txvLocalidad;
        public TextView txvProvincia;
        public TextView txvTlfFijo;
        public TextView txvTlfMovil;
        public TextView txvHorarioInicioA;
        public TextView txvHorarioFinA;
        public TextView txvHorarioInicioB;
        public TextView txvHorarioFinB;
        public TextView txvComercial;
        public TextView txvFormaPago;
        public TextView txvImporte;
        public TextView txvCajasSurtidas;
        public TextView txvCajasTotales;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        MyHolder holder = new MyHolder();

        if(v == null) { // Si el ítem no es reciclable, cargamos las Views
            if(getGroupType(groupPosition) == 0)      v = inflater.inflate(R.layout.cargar_item_ruta, parent, false);
            else if(getGroupType(groupPosition) == 1) v = inflater.inflate(R.layout.cargar_item_ruta_selected, parent, false);
            else if(getGroupType(groupPosition) == 2) v = inflater.inflate(R.layout.cargar_item_ruta_pendiente, parent, false);
            else                                      v = inflater.inflate(R.layout.cargar_item_ruta_selected_pendiente, parent, false);

            holder.txvId = (TextView) v.findViewById(R.id.txvId);
            holder.txvFecha = (TextView) v.findViewById(R.id.txvFecha);
            holder.txvSizeFacturas = (TextView) v.findViewById(R.id.txvSizeFacturas);
            v.setTag(holder);
        }
        else // Si el ítem se puede reciclar, obtenemos las Views
            holder = (MyHolder) v.getTag();

        holder.txvId.setText(rutas.get(groupPosition).IDRUTA);
        holder.txvFecha.setText(rutas.get(groupPosition).FECHAREPARTO);
        holder.txvSizeFacturas.setText(rutas.get(groupPosition).getSizeFacturas() + " facturas");

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, final View convertView, ViewGroup parent) {
        View v = convertView;
        MyHolderFactura holder = new MyHolderFactura();

        if(v == null) { // Si el ítem no es reciclable, cargamos las Views
            if(getChildType(groupPosition, childPosition) == 0) v = inflater.inflate(R.layout.cargar_item_factura, parent, false);
            else                                                v = inflater.inflate(R.layout.cargar_item_factura_pendiente, parent, false);

            holder.txvFactura = (TextView)v.findViewById(R.id.txvFactura);
            holder.txvCliente = (TextView)v.findViewById(R.id.txvCliente);
            holder.txvDireccion = (TextView)v.findViewById(R.id.txvDireccion);
            holder.txvCodigoPostal = (TextView)v.findViewById(R.id.txvCodigoPostal);
            holder.txvLocalidad = (TextView)v.findViewById(R.id.txvLocalidad);
            holder.txvProvincia = (TextView)v.findViewById(R.id.txvProvincia);
            holder.txvTlfFijo = (TextView)v.findViewById(R.id.txvTlfFijo);
            holder.txvTlfMovil = (TextView)v.findViewById(R.id.txvTlfMovil);
            holder.txvHorarioInicioA = (TextView)v.findViewById(R.id.txvHorarioInicioA);
            holder.txvHorarioFinA = (TextView)v.findViewById(R.id.txvHorarioFinA);
            holder.txvHorarioInicioB = (TextView)v.findViewById(R.id.txvHorarioInicioB);
            holder.txvHorarioFinB = (TextView)v.findViewById(R.id.txvHorarioFinB);
            holder.txvComercial = (TextView)v.findViewById(R.id.txvComercial);
            holder.txvFormaPago = (TextView)v.findViewById(R.id.txvFormaPago);
            holder.txvImporte = (TextView)v.findViewById(R.id.txvImporte);
            holder.txvCajasSurtidas = (TextView)v.findViewById(R.id.txvCajasSurtidas);
            holder.txvCajasTotales = (TextView)v.findViewById(R.id.txvCajasTotales);
            v.setTag(holder);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyHolderFactura holder = (MyHolderFactura) v.getTag();
                    interfaceRutas.launchEnviarFacturaActivity(rutas, holder.groupPosition, holder.childPosition);
                }
            });
        }
        else // Si el ítem se puede reciclar, obtenemos las Views
            holder = (MyHolderFactura) v.getTag();

        holder.childPosition = childPosition;
        holder.groupPosition = groupPosition;
        holder.txvFactura.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).NUMERO);
        holder.txvCliente.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).NOMBRECLIENTE + " (" + rutas.get(groupPosition).FACTURAS.get(childPosition).CODIGOCLIENTE + ")");
        holder.txvDireccion.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).DIRECCIONENTREGA);
        holder.txvCodigoPostal.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).CODIGOCLIENTE);
        holder.txvLocalidad.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).LOCALIDAD);
        holder.txvProvincia.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).PROVINCIA);
        holder.txvTlfFijo.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).TELEFONOFIJO);
        holder.txvTlfMovil.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).TELEFONOMOVIL);
        holder.txvHorarioInicioA.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).HORARIOINICIOA);
        holder.txvHorarioFinA.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).HORARIOFINA);
        holder.txvHorarioInicioB.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).HORARIOINICIOB);
        holder.txvHorarioFinB.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).HORARIOFINB);
        holder.txvComercial.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).COMERCIAL);
        holder.txvFormaPago.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).FORMAPAGO);
        holder.txvImporte.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).IMPORTE + " €");
        holder.txvCajasSurtidas.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).CAJASSURTIDAS);
        holder.txvCajasTotales.setText(rutas.get(groupPosition).FACTURAS.get(childPosition).CAJASTOTALES);

        return v;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        if ((lastExpanded != -1) && (lastExpanded != groupPosition)) // Si anteriormente había un grupo expandido,
            elv.collapseGroup(lastExpanded);                         // lo contraemos (únicamente puede haber un grupo expandido).
        expanded = lastExpanded = groupPosition; // Guardamos que el último grupo expandido sea el actual (para posteriormente saber cual contraer)
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        expanded = lastExpanded = -1; // Al contraer un grupo, indicamos que no hay grupos expandidos.
    }

    @Override
    public int getGroupType(int groupPosition) {
        if(groupPosition == expanded) {
            for(int i=0; i<rutas.get(groupPosition).FACTURAS.size(); i++)
                if(rutas.get(groupPosition).FACTURAS.get(i).pendiente) // Si hay facturas pendientes en el grupo actual...
                    return 3; // Grupo EXPANDIDO CON factura(s) pendiente(s)
            return 1; // Grupo EXPANDIDO SIN factura(s) pendiente(s)
        }
        else {
            for(int i=0; i<rutas.get(groupPosition).FACTURAS.size(); i++)
                if(rutas.get(groupPosition).FACTURAS.get(i).pendiente) // Si hay facturas pendientes en el grupo actual...
                    return 2; // Grupo CONTRAIDO CON factura(s) pendiente(s)
            return 0; // Grupo CONTRAIDO SIN factura(s) pendiente(s)
        }
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        if(rutas.get(groupPosition).FACTURAS.get(childPosition).pendiente)
            return 1; // PENDIENTE de envío
        return 0; // NO PENDIENTE (NORMAL)
    }

    @Override
    public int getGroupTypeCount() {
        return 4; // Contraído sin pendientes (0), expandido sin pendientes (1), contraído con pendientes (2), expandido con pendientes (3)
    }

    @Override
    public int getChildTypeCount() {
        return 2; // Factura pendiente de envío, factura NO pendiente de envío
    }

    @Override
    public int getGroupCount() {
        return this.rutas.size();
    }
    @Override
    public int getChildrenCount(int groupPosition) {
        return this.rutas.get(groupPosition).getSizeFacturas();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.rutas.get(groupPosition);
    }
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.rutas.get(groupPosition).FACTURAS.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
