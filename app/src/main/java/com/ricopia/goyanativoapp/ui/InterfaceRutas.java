package com.ricopia.goyanativoapp.ui;

import com.ricopia.goyanativoapp.obj.Ruta;
import java.util.ArrayList;


/**
 * Interfaz común para los fragmentos "Cargar Rutas" e "Informe Rutas".
 * Deben implementar la función launchEnviarFacturaActivity() para enviar los datos de la factura a la actividad de envío.
 */
public interface InterfaceRutas {
    void launchEnviarFacturaActivity(ArrayList<Ruta> rutas, int nRuta, int nFactura);
}
