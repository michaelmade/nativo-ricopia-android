package com.ricopia.goyanativoapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.melnykov.fab.FloatingActionButton;
import com.ricopia.goyanativoapp.obj.G;
import com.ricopia.goyanativoapp.EnviarFacturaActivity;
import com.ricopia.goyanativoapp.R;
import com.ricopia.goyanativoapp.obj.Ruta;
import com.ricopia.goyanativoapp.task.CargarRutasTask;
import java.util.ArrayList;


/**
 * Fragmento de informes, carga la lista de facturas que ya se han enviado con anterioridad
 */
public class InformeRutasFr extends Fragment implements InterfaceRutas {
    private CargarRutasTask cargarRutasTask;

    public static InformeRutasFr newInstance() {
        return new InformeRutasFr();
    }

    // Constructor por defecto (obligatorio)
    public InformeRutasFr() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.informe__tab_rutas, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ExpandableListView elvRutas = (ExpandableListView) getView().findViewById(R.id.elvRutas);
        RutaAdapterInforme adapter = new RutaAdapterInforme(getView().getContext(), new ArrayList<Ruta>(), elvRutas, this);
        adapter.setTxvResumen(getView()); // Pasamos el fragmento para que pueda recuperar todos los TextViews del informe general
        elvRutas.setAdapter(adapter);
        // Inicializamos la tarea de carga de rutas de informe
        cargarRutasTask = new CargarRutasTask(getActivity().getApplicationContext(), adapter, G.OP_ACCION_INFORME);

        ProgressBar prb = (ProgressBar) getView().findViewById(R.id.prbCargarRutas);
        LinearLayout lnlInforme = (LinearLayout) getView().findViewById(R.id.lnlInforme);
        FloatingActionButton fab = (FloatingActionButton) getView().findViewById(R.id.fab); // Botón de sincronización

        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarRutasTask.loadFromNet(); // Siempre que se pulse el botón se trata de recargar los datos del servidor, si no desde fihcero.
            }
        });

        cargarRutasTask.setPrbCargarRutas(prb, lnlInforme); // Pasamos el progressBar a la tarea de carga (para no dar sensación de bloqueo).
        updateData(false); // Inicialmente cargamos los datos desde el fichero, si nunca se han cargado -> cargamos desde el servidor
    }


    /**
     * Inicia la tarea de carga de rutas ya enviadas. Si tratamos de cargar rutas desde fichero y no se encuentra se cargarán las rutas desde el servidor.
     *
     * @param fromNet Carga desde el servidor (true) o desde el fichero de la memoria interna (false)
     */
    private void updateData(boolean fromNet) {
        if(fromNet)
            cargarRutasTask.loadFromNet();
        else if(!cargarRutasTask.show(true)) // Si no carga las rutas del fichero, forzamos cargar rutas de red
            cargarRutasTask.loadFromNet();
    }

    @Override
    public void launchEnviarFacturaActivity(ArrayList<Ruta> rutas, int nRuta, int nFactura) {
        Intent i = new Intent(getView().getContext(), EnviarFacturaActivity.class);
        i.putExtra("rutas", rutas);
        i.putExtra("nRuta", nRuta);
        i.putExtra("nFactura", nFactura);
        i.putExtra("nueva", false); // Indicamos que la factura es nueva (true) y, por lo tanto, no proviene de informe (false)
        startActivityForResult(i, G.ACTIVITY_ENVIARFACTURA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == G.ACTIVITY_ENVIARFACTURA) {
            // Recargamos rutas desde el servidor
            if(resultCode == G.UPDATE_FROM_NET)
                updateData(true);
            // Recargamos rutas desde el fichero
            else if(resultCode == G.UPDATE_FROM_FILE)
                updateData(false);
        }
    }
}
