package com.ricopia.goyanativoapp.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * Controlador de pestañas.
 *   1. Cargar rutas
 *   2. Informe de rutas
 */
public class MyFragmentPagerAdapter extends FragmentPagerAdapter {
    private String tabTitles[] = new String[] { "Cargar rutas", "Informe de rutas"};

    public MyFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return this.tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return this.tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: return CargarRutasFr.newInstance();
            case 1: return InformeRutasFr.newInstance();
        }
        return null;
    }
}
