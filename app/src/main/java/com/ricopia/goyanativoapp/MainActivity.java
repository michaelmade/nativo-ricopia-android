package com.ricopia.goyanativoapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import com.ricopia.goyanativoapp.ui.MyFragmentPagerAdapter;
import java.io.Serializable;


/**
 * Actividad principal, en ésta actividad se cargarán las pestañas.
 */
public class MainActivity extends AppCompatActivity implements Serializable {

    public static MainActivity mainActivity; // Declarada como pública para cerrar actividad en caso de autenticación incorrecta

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(loginOk()) { // Si ya se han almacenado credenciales (aunque no se hayan comprobado), mostramos la pantalla principal
            setContentView(R.layout.activity_main);

            ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
            viewPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager()));
            //viewPager.setCurrentItem(1); // Para cambiar la pestaña que se muestra de primeras
            TabLayout tabLayout = (TabLayout) findViewById(R.id.appbartabs);
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE); // Se puede cambiar de una pestaña a otra deslizando el dedo
            tabLayout.setupWithViewPager(viewPager);
        }

        mainActivity = this;
    }

    /**
     * Indica si ya se han almacenado credenciales (aunque no se hayan comprobado).
     *
     * @return Devuelve si hay login almacenado (true) o no (false).
     */
    private boolean loginOk() {
        SharedPreferences prefs = getSharedPreferences("LoginData", Context.MODE_PRIVATE);
        LoginActivity.USER = prefs.getString("user", null);
        LoginActivity.PASS = prefs.getString("pass", null);

        if((LoginActivity.USER != null) && (LoginActivity.PASS != null))
            return true;
        else {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            return false;
        }
    }

    /**
     * Abre pantalla de login para solicitar credenciales y cierra actividad principal (se volverá a abrir una vez se reciban credenciales).
     */
    public void launchLoginActivity() {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }
}
