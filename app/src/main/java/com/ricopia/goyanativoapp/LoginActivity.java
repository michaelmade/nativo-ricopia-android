package com.ricopia.goyanativoapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.ricopia.goyanativoapp.ui.dialog.DialogInfo;


/**
 * Actividad de Login, solicita y almacena credenciales de usuario en las preferencias de la App (gestionado por el sistema Android).
 */
public class LoginActivity extends AppCompatActivity {

    public static String USER;
    public static String PASS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Action Bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);

        // Botón Acceder/Login
        Button btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TextView edtUser = (TextView) findViewById(R.id.edtUser);
                TextView edtPass = (TextView) findViewById(R.id.edtPass);
                SharedPreferences prefs = getSharedPreferences("LoginData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("user", edtUser.getText().toString());
                editor.putString("pass", edtPass.getText().toString());
                editor.commit(); // Guardamos datos recibidos en preferencias de la app (gestionado por el sistema Android)
                startActivity(new Intent(getApplicationContext(), MainActivity.class)); // Abrimos actividad principal
                finish(); // Eliminamos actividad Login, ya no es necesaria
            }
        });

        SharedPreferences prefs = getSharedPreferences("LoginData", Context.MODE_PRIVATE);
        // Si se encuentran datos almacenados previamente y hemos llegado a esta pantalla es porque las credenciales
        // no son correctas o han cambiado en el servidor, solicitamos credenciales correctas.
        if((prefs.getString("user", null) != null) || (prefs.getString("pass", null) != null)) {
            DialogInfo d = new DialogInfo();
            d.set("Autenticación incorrecta", "Introduzca un usuario y contraseña válidos.");
            d.show(getSupportFragmentManager(), "loginKO"); // Notificamos al usuario mediante un diálogo
        }
    }
}
