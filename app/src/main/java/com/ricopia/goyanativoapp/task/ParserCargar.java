package com.ricopia.goyanativoapp.task;

import android.util.Log;
import com.ricopia.goyanativoapp.obj.Factura;
import com.ricopia.goyanativoapp.obj.Ruta;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * A partir de un XML pasado por un Stream se obtiene una lista de rutas en formato Java
 */
public class ParserCargar {

    private InputStream source;
    protected Element xml; // Elemento raíz

    public ParserCargar(InputStream source) {
        this.source = source;
    }

    /**
     * Parsea XML procedente del constructor.
     *
     * @return Si se parseó correctamente (true) o si hubo algún problema (false).
     */
    public boolean run() {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document dom = builder.parse(source);
            this.xml = dom.getDocumentElement();
            Log.i("XML", "XML parseado correctamente");
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e("XML", "Error al parsear XML: "+ex.getLocalizedMessage()+" // "+ex.getMessage());
            return false;
        }
    }

    /**
     * Devuelve la lista de rutas procedentes del XML.
     *
     * @return Lista de rutas.
     */
    public ArrayList<Ruta> getRutas() {
        ArrayList<Ruta> rutas = new ArrayList<>();
        NodeList rutasXml = xml.getElementsByTagName("RUTA"); // Buscamos los nodos "RUTA"

        for(int i=0; i<rutasXml.getLength(); i++) {
            Ruta ruta = new Ruta();
            NodeList nodosRuta = rutasXml.item(i).getChildNodes();

            // RUTAS
            for(int j=0; j<nodosRuta.getLength(); j++) {
                // CAMPOS RUTA
                Node n = nodosRuta.item(j);
                if(n.getNodeName().equals("ID"))                ruta.ID = n.getTextContent();
                else if(n.getNodeName().equals("IDRUTA"))       ruta.IDRUTA = n.getTextContent();
                else if(n.getNodeName().equals("FECHAREPARTO")) ruta.FECHAREPARTO = n.getTextContent();
                else if(n.getNodeName().equals("FACTURAS")) {
                    NodeList nodosFacturas = n.getChildNodes();
                    // FACTURAS
                    for(int k=0; k<nodosFacturas.getLength(); k++) {
                        NodeList nodosFactura = nodosFacturas.item(k).getChildNodes();
                        Factura nuevaFactura = new Factura();
                        // CAMPOS FACTURA
                        for(int l=0; l<nodosFactura.getLength(); l++) {
                            Node nF = nodosFactura.item(l);
                            if(nF.getNodeName().equals("IDFACTURA"))               nuevaFactura.IDFACTURA = nF.getTextContent();
                            else if(nF.getNodeName().equals("NUMERO"))             nuevaFactura.NUMERO = nF.getTextContent();
                            else if(nF.getNodeName().equals("CODIGOCLIENTE"))      nuevaFactura.CODIGOCLIENTE = nF.getTextContent();
                            else if(nF.getNodeName().equals("NOMBRECLIENTE"))      nuevaFactura.NOMBRECLIENTE = nF.getTextContent();
                            else if(nF.getNodeName().equals("DIRECCIONENTREGA"))   nuevaFactura.DIRECCIONENTREGA = nF.getTextContent();
                            else if(nF.getNodeName().equals("LOCALIDAD"))          nuevaFactura.LOCALIDAD = nF.getTextContent();
                            else if(nF.getNodeName().equals("PROVINCIA"))          nuevaFactura.PROVINCIA = nF.getTextContent();
                            else if(nF.getNodeName().equals("CODIGOPOSTAL"))       nuevaFactura.CODIGOPOSTAL = nF.getTextContent();
                            else if(nF.getNodeName().equals("TELEFONOFIJO"))       nuevaFactura.TELEFONOFIJO = nF.getTextContent();
                            else if(nF.getNodeName().equals("TELEFONOMOVIL"))      nuevaFactura.TELEFONOMOVIL = nF.getTextContent();
                            else if(nF.getNodeName().equals("HORARIOINICIOA"))     nuevaFactura.HORARIOINICIOA = nF.getTextContent();
                            else if(nF.getNodeName().equals("HORARIOFINA"))        nuevaFactura.HORARIOFINA = nF.getTextContent();
                            else if(nF.getNodeName().equals("HORARIOINICIOB"))     nuevaFactura.HORARIOINICIOB = nF.getTextContent();
                            else if(nF.getNodeName().equals("HORARIOFINB"))        nuevaFactura.HORARIOFINB = nF.getTextContent();
                            else if(nF.getNodeName().equals("COMERCIAL"))          nuevaFactura.COMERCIAL = nF.getTextContent();
                            else if(nF.getNodeName().equals("FORMAPAGO"))          nuevaFactura.FORMAPAGO = nF.getTextContent();
                            else if(nF.getNodeName().equals("IMPORTE"))            nuevaFactura.IMPORTE = nF.getTextContent();
                            else if(nF.getNodeName().equals("CAJASSURTIDAS"))      nuevaFactura.CAJASSURTIDAS = nF.getTextContent();
                            else if(nF.getNodeName().equals("CAJASTOTALES"))       nuevaFactura.CAJASTOTALES = nF.getTextContent();
                            else if(nF.getNodeName().equals("ESTATUS"))            nuevaFactura.ESTATUS = nF.getTextContent();
                            else if(nF.getNodeName().equals("PAGOPARCIAL"))        nuevaFactura.PAGOPARCIAL = nF.getTextContent();
                            else if(nF.getNodeName().equals("PAGOPARCIALCHEQUE"))  nuevaFactura.PAGOPARCIALCHEQUE = nF.getTextContent();
                            else if(nF.getNodeName().equals("PAGOANTERIOR"))       nuevaFactura.PAGOANTERIOR = nF.getTextContent();
                            else if(nF.getNodeName().equals("PAGOANTERIORCHEQUE")) nuevaFactura.PAGOANTERIORCHEQUE = nF.getTextContent();
                            else if(nF.getNodeName().equals("TOTALEFECTIVO"))      nuevaFactura.TOTALEFECTIVO = nF.getTextContent();
                            else if(nF.getNodeName().equals("TOTALCHEQUE"))        nuevaFactura.TOTALCHEQUE = nF.getTextContent();
                            else if(nF.getNodeName().equals("OBSERVACIONES"))      nuevaFactura.OBSERVACIONES = nF.getTextContent();
                        }
                        ruta.FACTURAS.add(nuevaFactura); // Agregamos factura
                    }
                }
            }
            rutas.add(ruta); // Agregamos ruta
        }
        return rutas; // Devolvemos lista de rutas
    }
}
