package com.ricopia.goyanativoapp.task;

import android.os.AsyncTask;
import android.util.Log;
import com.ricopia.goyanativoapp.LoginActivity;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Envía una cadena con formato XML (tiene que generarse previamente) con rutas procesadas para guardar en el servidor.
 * Devuelve el resultado a la función setDataReceived() para su posterior interpretación.
 */
public class EnviarRutasNet extends AsyncTask<URL, Void, InputStream> {

    private EnviarRutasTask enviarRutasTask;
    private boolean login;
    private String rutasXml;

    public EnviarRutasNet(EnviarRutasTask enviarRutasTask, String rutasXml) {
        this.login = true;
        this.enviarRutasTask = enviarRutasTask;
        this.rutasXml = rutasXml;
    }

    @Override
    protected InputStream doInBackground(URL... urls) {
        String data = "";
        HttpURLConnection con = null;
        String toServer = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                "<s:Body>" +
                "<ActualizarRutas xmlns=\"http://192.168.50.124:8022/\">" +
                    "<xmlruta>" +
                        "<![CDATA[" +
                            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                            "<xmlsalida xmlns=\"http://192.168.50.124:8022/\">" +
                                "<RUTAS xmlns=''>"+rutasXml+"</RUTAS>" +
                            "</xmlsalida>" +
                        "]]>" +
                    "</xmlruta>"+
                "<TRANSPORTISTA>"+LoginActivity.USER+"</TRANSPORTISTA>"+
                "<PASS>"+LoginActivity.PASS+"</PASS>"+
                "<USUARIOWEB>R1c0p14@2015</USUARIOWEB>"+
                "<PASSWEB>#R1c0p14@hydeboorsatetiny#1903</PASSWEB>"+
                "</ActualizarRutas>"+
                "</s:Body>" +
                "</s:Envelope>";

        try {
            con = (HttpURLConnection) urls[0].openConnection();
            con.setRequestMethod("POST");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestProperty("Content-type", "text/xml; charset=UTF-8");

            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(toServer);
            writer.flush();
            writer.close();

            // con.getResponseCode();
            InputStream is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = "";
            while((line = br.readLine()) != null)
                data += line;

            return new ByteArrayInputStream(data.getBytes());
        }
        // Las credenciales de usuario no son correctas
        catch (FileNotFoundException ex) {
            con.disconnect();
            login = false;
            Log.e("FileNotFoundException", "###### "+ex.getMessage());
        }
        // No hay conexión a Internet o hay algún error desconocido
        catch(Exception ex) {
            con.disconnect();
            Log.e("NET", "###### " + ex.getLocalizedMessage());
        }

        return null;
    }

    @Override
    protected void onPostExecute(InputStream data) {
        this.enviarRutasTask.setDataReceived(data, login); // Se devuelve el resultado a la tarea
    }
}
