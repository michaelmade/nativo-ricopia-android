package com.ricopia.goyanativoapp.task;

import android.os.AsyncTask;
import android.util.Log;
import com.ricopia.goyanativoapp.LoginActivity;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Solicita al servidor rutas asignadas al usuario pasado.
 * Devuelve las rutas como resultado a la función setDataReceived() para su posterior interpretación.
 */
public class CargarRutasNet extends AsyncTask<URL, Void, InputStream> {

    private CargarRutasTask cargarRutasTask;
    private boolean login;

    public CargarRutasNet(CargarRutasTask cargarRutasTask) {
        this.cargarRutasTask = cargarRutasTask;
        this.login = true;
    }

    @Override
    protected InputStream doInBackground(URL... urls) {
        String data = "";
        HttpURLConnection con = null;
        String toServer = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                "<s:Body>" +
                "<"+cargarRutasTask.OP_ACCION +" xmlns=\"http://192.168.50.124:8022/\">" +
                "<TRANSPORTISTA>"+LoginActivity.USER+"</TRANSPORTISTA>"+
                "<PASS>"+LoginActivity.PASS+"</PASS>"+
                "<USUARIOWEB>R1c0p14@2015</USUARIOWEB>"+
                "<PASSWEB>#R1c0p14@hydeboorsatetiny#1903</PASSWEB>"+
                "</"+cargarRutasTask.OP_ACCION +">"+
                "</s:Body>" +
                "</s:Envelope>";

        try {
            con = (HttpURLConnection) urls[0].openConnection();
            con.setRequestMethod("POST");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestProperty("Content-type", "text/xml; charset=UTF-8");

            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(toServer);
            writer.flush();
            writer.close();

            //con.getResponseCode();
            InputStream is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = "";
            while((line = br.readLine()) != null)
                data += line;

            return new ByteArrayInputStream(data.getBytes());
        }
        // Las credenciales de usuario no son correctas
        catch (FileNotFoundException ex) {
            con.disconnect();
            login = false;
            Log.e("FileNotFoundException", "###### "+ex.getMessage());
        }
        // No hay conexión a Internet o hay algún error desconocido
        catch(Exception ex) {
            con.disconnect();
            Log.e("NET", "###### " + ex.getLocalizedMessage());
        }

        return null;
    }

    @Override
    protected void onPostExecute(InputStream data) {
        this.cargarRutasTask.setDataReceived(data, login); // Se devuelve el resultado a la tarea
    }
}
