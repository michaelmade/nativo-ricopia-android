package com.ricopia.goyanativoapp.task;

import android.content.Context;
import android.widget.Toast;
import java.io.InputStream;


/**
 * A partir de un XML pasado por un Stream se obtiene el resultado de una petición al servidor
 */
public class ParserActualizar extends ParserCargar {

    public ParserActualizar(InputStream source) {
        super(source);
    }

    /**
     * Comprueba y notifica al usuario en caso de que haya habido algún error en la petición realizada (no de conexión).
     *
     * @param context Conexto necesario para notificar al usuario mediante Toasts.
     * @return        Si el resultado es correcto (true) o se ha recibido algún error (false).
     */
    public boolean isOk(Context context) {
        try {
            if(!run())
                Toast.makeText(context, "ERROR: no se pudo parsear el XML recibido (isOk())", Toast.LENGTH_LONG).show();
            else if(xml.getElementsByTagName("ActualizarRutasResult").item(0).getFirstChild().getTextContent().equals("OK"))
                return true;
            else {
                String str1 = xml.getElementsByTagName("ActualizarRutasResult").item(0).getChildNodes().item(0).getTextContent();
                String str2 = xml.getElementsByTagName("ActualizarRutasResult").item(0).getChildNodes().item(1).getTextContent();
                Toast.makeText(context, str1+": "+str2, Toast.LENGTH_LONG).show(); // Mostramos al usuario el error recibido del servidor
            }
        } catch (Exception ex) {}

        return false;
    }
}
