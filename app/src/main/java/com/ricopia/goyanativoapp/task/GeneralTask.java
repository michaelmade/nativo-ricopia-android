package com.ricopia.goyanativoapp.task;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.ricopia.goyanativoapp.obj.G;
import com.ricopia.goyanativoapp.obj.Ruta;

import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class GeneralTask {

    protected String FILE_PATH;
    protected ArrayList<Ruta> rutas;

    protected Context context;
    protected View prb;
    protected View v;

    public GeneralTask(Context context, ArrayList<Ruta> rutas, String opAccion) {
        this.context = context;
        this.rutas = rutas;
        if(opAccion.equals(G.OP_ACCION_CARGAR)) FILE_PATH = G.FILE_PATH_CARGAR;
        else         /* Global.OP_ACCION_INFORME */  FILE_PATH = G.FILE_PATH_INFORME;
    }

    /**
     * Muestra el ProgressBar y oculta lo demás.
     */
    protected void showProgressBar() {
        v.setVisibility(View.GONE);
        prb.setVisibility(View.VISIBLE);
    }


    /**
     * Oculta el ProgressBar y vuelve a mostrar lo demás.
     */
    protected void hiddeProgressBar() {
        v.setVisibility(View.VISIBLE);
        prb.setVisibility(View.GONE);
    }

    /**
     * Recibe las Views correspondientes al ProgressBar y ViewGroup que contiene lo que se debe ocultar cuando se muestra el ProgressBar
     *
     * @param prb ProgressBar a mostrar
     * @param v   View a ocultar
     */
    public void setPrbCargarRutas(View prb, View v) {
        this.prb = prb;
        this.v = v;
    }

    /**
     * Guarda el objeto Rutas en el fichero correspondiente de la memoria interna.
     */
    protected void saveToFile() {
        try
        {
            ObjectOutputStream oos = new ObjectOutputStream(context.openFileOutput(FILE_PATH, Context.MODE_PRIVATE));
            oos.writeObject(rutas);
            oos.close();
        }
        catch (Exception ex)
        {
            Toast.makeText(context, "Error al guardar el fichero de rutas de la memoria interna", Toast.LENGTH_LONG).show();
        }
    }
}
