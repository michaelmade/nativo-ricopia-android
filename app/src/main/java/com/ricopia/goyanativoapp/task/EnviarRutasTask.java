package com.ricopia.goyanativoapp.task;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.ricopia.goyanativoapp.EnviarFacturaActivity;
import com.ricopia.goyanativoapp.MainActivity;
import com.ricopia.goyanativoapp.obj.G;
import com.ricopia.goyanativoapp.obj.Ruta;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


/**
 * Clase para enviar rutas con facturas procesadas al servidor, tanto desde la pestaña "Cargar Rutas" como de "Informe de Rutas"
 */
public class EnviarRutasTask extends GeneralTask implements Serializable {

    private EnviarFacturaActivity enviarFacturaActivity;
    private CargarRutasTask cargarRutasTask;

    /**
     * Inicializa la tarea de envío.
     *
     * @param context               Contexto para las toast.
     * @param enviarFacturaActivity Actividad de envio de facturas, necesaria para cerrar la actividad una vez se haya enviado la factura.
     * @param opAccion              Si se envía la ruta desde rutas asignadas o desde informes.
     */
    public EnviarRutasTask(Context context, EnviarFacturaActivity enviarFacturaActivity, String opAccion) {
        super(context, new ArrayList<Ruta>(), opAccion);
        this.enviarFacturaActivity = enviarFacturaActivity;
    }

    /**
     * Envía las facturas pendientes (procesadas sin conexión) al servidor.
     *
     * @param rutas           Rutas pendientes de envío.
     * @param cargarRutasTask Tarea de carga, necesaria para posteriormente refrescar las rutas del servidor.
     * @return                True si la operación fue correcta, False si se produjo algún error al abrir la conexión.
     */
    public boolean updateOfflineToNet(ArrayList<Ruta> rutas, CargarRutasTask cargarRutasTask) {
        showProgressBar();
        this.cargarRutasTask = cargarRutasTask;
        this.rutas = rutas;
        try {
            new EnviarRutasNet(this, Ruta.toXML(this.rutas)).execute(new URL("http://ricopia-nativo.goya.com:8022/conversor.asmx?op=ActualizarRutas"));
        }
        catch (MalformedURLException ex) {
            Log.e("NET", "###### URL mal formada: " + ex.getLocalizedMessage());
            hiddeProgressBar();
            return false;
        }

        return true;
    }

    /**
     * Envía la factura procesada y pendientes (si las hay) al servidor.
     *
     * @param rutas Rutas con facturas para enviar.
     */
    public void updateToNet(ArrayList<Ruta> rutas) {
        showProgressBar();
        this.rutas = rutas;
        saveToFile(); // Guardamos las rutas antes de enviarlas al servidor (por si se produce algún error de conexión).
        try {
            new EnviarRutasNet(this, Ruta.toXML(rutas)).execute(new URL("http://ricopia-nativo.goya.com:8022/conversor.asmx?op=ActualizarRutas"));
        }
        catch (MalformedURLException ex) {
            Log.e("NET", "###### URL mal formada: " + ex.getLocalizedMessage());
            hiddeProgressBar();
        }
    }

    /**
     * Función que llamará la clase EnviarRutasNet cuando reciba una respuesta del servidor con los datos solicitados.
     *
     * @param data  Datos procedentes del servidor.
     * @param login True si las credenciales son correctas, False si NO son correctas.
     */
    public void setDataReceived(InputStream data, boolean login) {
        if(data != null) { // Si recibimos datos
            if(new ParserActualizar(data).isOk(context)) {
                Toast.makeText(context, "Facturas enviadas correctamente", Toast.LENGTH_LONG).show();
                if(enviarFacturaActivity == null) // Los datos se han enviado sin la actividad de envío, es decir, se han enviado facturas pendientes
                    cargarRutasTask.loadFromNet(false);
                else {
                    enviarFacturaActivity.setResult(G.UPDATE_FROM_NET); // Indicamos que se recargen los datos del servidor, las facturas pendientes deberían desaparecer
                    enviarFacturaActivity.finish(); // Finalizamos actividad de envío y damos paso a la actividad principal con los datos actualizados
                }
            }
        }
        else { // Si no vienen datos
            if(!login) { // Las credenciales no son correctas
                try {enviarFacturaActivity.finish();} // Cerramos actividad de envío
                catch (Exception ex) {}
                MainActivity.mainActivity.launchLoginActivity(); // Abrimos actividad de Login para solicitar credenciales correctas
            }
            else { // No se dispone de una conexión a internet
                Toast.makeText(context, "No hay conexión a internet, las facturas quedan pendientes de envío", Toast.LENGTH_LONG).show();
                try {
                    enviarFacturaActivity.setResult(G.UPDATE_FROM_FILE); // Al no haber internet, tenemos que cargar desde fichero (modo offline)
                    enviarFacturaActivity.finish();
                } catch (Exception ex) {}
            }
        }
        hiddeProgressBar();
    }
}
