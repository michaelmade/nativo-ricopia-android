package com.ricopia.goyanativoapp.task;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.ricopia.goyanativoapp.MainActivity;
import com.ricopia.goyanativoapp.obj.G;
import com.ricopia.goyanativoapp.obj.Ruta;
import com.ricopia.goyanativoapp.ui.RutaAdapter;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


/**
 * Clase para cargar (de forma asíncrona) rutas, tanto desde la pestaña "Cargar Rutas" como de "Informe de Rutas"
 */
public class CargarRutasTask extends GeneralTask implements Serializable {

    private RutaAdapter adapter; // Adaptador sobre el que se recargan las rutas

    public String OP_ACCION; // OP_ACCION_CARGAR ó OP_ACCION_INFORME

    /**
     * Inicializa la tarea de carga.
     *
     * @param context  Contexto para las toast.
     * @param adapter  Adaptador sobre el que se recargan las rutas
     * @param opAccion Si se envía la ruta desde rutas asignadas o desde informes.
     */
    public CargarRutasTask(Context context, RutaAdapter adapter, String opAccion) {
        super(context, new ArrayList<Ruta>(), opAccion);
        this.adapter = adapter;
        this.OP_ACCION = opAccion;
    }

    /**
     * Carga datos del servidor por primera vez.
     */
    public void loadFromNet() {
        loadFromNet(true);
    }

    /**
     * Carga datos del servidor y devuleve el resultado a la función setDataReceived().
     *
     * @param firstTime Si es la primera vez que cargamos los datos (onCreate) pasamos (true), si no (false).
     */
    public void loadFromNet(boolean firstTime) {
        try {
            showProgressBar();

            if(rutasPendientes() && firstTime) {
                EnviarRutasTask enviarRutasTask = new EnviarRutasTask(context, null, FILE_PATH);
                enviarRutasTask.setPrbCargarRutas(prb, v);
                enviarRutasTask.updateOfflineToNet(rutas, this);
            }
            else {
                new CargarRutasNet(this).execute(new URL("http://ricopia-nativo.goya.com:8022/conversor.asmx?op="+ OP_ACCION));
            }
        }
        catch (MalformedURLException ex) {
            Log.e("NET", "###### URL mal formada: " + ex.getLocalizedMessage());
            hiddeProgressBar();
        }
    }

    /**
     * Lee y comprueba en el fichero interno de rutas si hay alguna factura pendiente de envío.
     *
     * @return True SI hay facturas pendientes, False si NO hay facturas pendientes.
     */
    private boolean rutasPendientes() {
        try
        {
            ObjectInputStream ios = new ObjectInputStream(context.openFileInput(FILE_PATH));
            rutas = (ArrayList<Ruta>) ios.readObject();
            ios.close();
        }
        catch (Exception ex)
        {
            rutas = null;
            hiddeProgressBar();
            return false;
        }

        for(int i=0; i<rutas.size(); i++)
            for(int j=0; j<rutas.get(i).FACTURAS.size(); j++)
                if(rutas.get(i).FACTURAS.get(j).pendiente)
                    return true;

        return false;
    }

    /**
     * Función que llamará la clase EnviarRutasNet cuando reciba una respuesta del servidor con los datos solicitados.
     *
     * @param data  Datos procedentes del servidor.
     * @param login True si las credenciales son correctas, False si NO son correctas.
     */
    public void setDataReceived(InputStream data, boolean login) {
        if(data != null) { // Si recibimos datos
            ParserCargar parserCargar = new ParserCargar(data);
            parserCargar.run();
            rutas = parserCargar.getRutas();
            saveToFile();
            show(false);
        }
        else { // Si no vienen datos
            if(!login) // Las credenciales no son correctas
                MainActivity.mainActivity.launchLoginActivity(); // Abrimos actividad de Login para solicitar credenciales correctas
            else { // No se dispone de una conexión a internet
                if(show(true)) { // Tratamos de cargar las rutas desde el fichero interior
                    hiddeProgressBar();
                    Toast.makeText(context, "No hay conexión a internet, las rutas cargadas pueden NO ESTAR ACTUALIZADAS", Toast.LENGTH_LONG).show();
                }
                else { // Si el fichero interno no existe, simplemente notificamos al usuario, no podemos hacer más hasta que no haya conexión a internet
                    hiddeProgressBar();
                    Toast.makeText(context, "No hay conexión a internet, ni rutas previamente descargadas", Toast.LENGTH_LONG).show();
                }
            }
        }
        hiddeProgressBar();
    }

    /**
     * Actualiza el adaptador con los datos cargados desde el fichero interno o desde la memoria RAM.
     *
     * @param fromFile Si los datos se cargarán del fichero (true) o desde RAM (false).
     * @return         Si se han podido cargar datos de la fuente indicada (true) o no (false).
     */
    public boolean show(boolean fromFile) {
        showProgressBar();
        if(fromFile) { // Cargamos datos desde el fichero interno
            try
            {
                ObjectInputStream ios = new ObjectInputStream(context.openFileInput(FILE_PATH));
                rutas = (ArrayList<Ruta>) ios.readObject(); // Cargamos las rutas del fichero en RAM
                ios.close();
            }
            catch (Exception ex) // Si hay algún con el fichero, indicamos que no se ha podido cargar y destruimos las rutas de RAM
            {
                rutas = null;
                return false;
            }
        }

        if(rutas != null) { // Si hay rutas cargadas en RAM
            adapter.rutas = rutas;
            adapter.notifyDataSetChanged();

            if(rutas.size() == 0) { // Si no hay ninguna ruta, avisamos al usuario
                if(OP_ACCION.equals(G.OP_ACCION_INFORME))
                    Toast.makeText(context, "No hay informes asignados", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(context, "No hay rutas asignados", Toast.LENGTH_LONG).show();
            }
        }
        else
            return false;

        hiddeProgressBar();
        return true;
    }
}
