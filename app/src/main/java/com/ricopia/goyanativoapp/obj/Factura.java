package com.ricopia.goyanativoapp.obj;

import java.io.Serializable;


/**
 * Objeto que almacena los datos de una factura y si se encuentra pendiente de ser enviada al servidor.
 */
public class Factura implements Serializable {

    public String IDFACTURA;
    public String NUMERO;
    public String CODIGOCLIENTE;
    public String NOMBRECLIENTE;
    public String DIRECCIONENTREGA;
    public String LOCALIDAD;
    public String PROVINCIA;
    public String CODIGOPOSTAL;
    public String TELEFONOFIJO;
    public String TELEFONOMOVIL;
    public String HORARIOINICIOA;
    public String HORARIOFINA;
    public String HORARIOINICIOB;
    public String HORARIOFINB;
    public String COMERCIAL;
    public String FORMAPAGO;
    public String IMPORTE;
    public String CAJASSURTIDAS;
    public String CAJASTOTALES;
    public String ESTATUS;
    public String PAGOPARCIAL;
    public String PAGOPARCIALCHEQUE;
    public String PAGOANTERIOR;
    public String PAGOANTERIORCHEQUE;
    public String TOTALEFECTIVO;
    public String TOTALCHEQUE;
    public String OBSERVACIONES;
    public boolean pendiente;

    /**
     * Constructor de factura, establece que la factura no está pendiente inicialmente.
     */
    public Factura() {
        this.pendiente = false;
    }

    /**
     * Convierte la factura en una cadena con estructura XML lista para ser enviada.
     *
     * @return Cadena con estructura XML.
     */
    public String toXML() {
        return
            "<FACTURA>"+
                "<IDFACTURA>"+this.IDFACTURA+"</IDFACTURA>"+
                "<NUMERO>"+this.NUMERO+"</NUMERO>"+
                "<CODIGOCLIENTE>"+this.CODIGOCLIENTE+"</CODIGOCLIENTE>"+
                "<NOMBRECLIENTE>"+this.NOMBRECLIENTE+"</NOMBRECLIENTE>"+
                "<DIRECCIONENTREGA>"+this.DIRECCIONENTREGA+"</DIRECCIONENTREGA>"+
                "<LOCALIDAD>"+this.LOCALIDAD+"</LOCALIDAD>"+
                "<PROVINCIA>"+this.PROVINCIA+"</PROVINCIA>"+
                "<CODIGOPOSTAL>"+this.CODIGOPOSTAL+"</CODIGOPOSTAL>"+
                "<TELEFONOFIJO>"+this.TELEFONOFIJO+"</TELEFONOFIJO>"+
                "<TELEFONOMOVIL>"+this.TELEFONOMOVIL+"</TELEFONOMOVIL>"+
                "<HORARIOINICIOA>"+this.HORARIOINICIOA+"</HORARIOINICIOA>"+
                "<HORARIOFINA>"+this.HORARIOFINA+"</HORARIOFINA>"+
                "<HORARIOINICIOB>"+this.HORARIOINICIOB+"</HORARIOINICIOB>"+
                "<HORARIOFINB>"+this.HORARIOFINB+"</HORARIOFINB>"+
                "<COMERCIAL>"+this.COMERCIAL+"</COMERCIAL>"+
                "<FORMAPAGO>"+this.FORMAPAGO+"</FORMAPAGO>"+
                "<IMPORTE>"+this.IMPORTE+"</IMPORTE>"+
                "<CAJASSURTIDAS>"+this.CAJASSURTIDAS+"</CAJASSURTIDAS>"+
                "<CAJASTOTALES>"+this.CAJASTOTALES+"</CAJASTOTALES>"+
                "<ESTATUS>"+this.ESTATUS+"</ESTATUS>"+
                "<PAGOPARCIAL>"+this.PAGOPARCIAL+"</PAGOPARCIAL>"+
                "<PAGOPARCIALCHEQUE>"+this.PAGOPARCIALCHEQUE+"</PAGOPARCIALCHEQUE>"+
                "<PAGOANTERIOR>"+this.PAGOANTERIOR+"</PAGOANTERIOR>"+
                "<PAGOANTERIORCHEQUE>"+this.PAGOANTERIORCHEQUE+"</PAGOANTERIORCHEQUE>"+
                "<TOTALEFECTIVO>"+this.TOTALEFECTIVO+"</TOTALEFECTIVO>"+
                "<TOTALCHEQUE>"+this.TOTALCHEQUE+"</TOTALCHEQUE>"+
                "<OBSERVACIONES>"+this.OBSERVACIONES+"</OBSERVACIONES>"+
            "</FACTURA>";
    }

    /**
     * Establece la factura como nula. Campos a "0" (excepto pagos anteriores) y ESTATUS a "NULO".
     *
     * @param anteriorEfectivo Importe anterior efectivo.
     * @param anteriorTalon    Importe anterior talón.
     * @param observaciones    Observaciones.
     */
    public void setFacturaNula(float anteriorEfectivo, float anteriorTalon, String observaciones) {
        this.ESTATUS            = "NULO";
        this.TOTALEFECTIVO      = "0";
        this.TOTALCHEQUE        = "0";
        this.PAGOPARCIAL        = "0";
        this.PAGOPARCIALCHEQUE  = "0";
        this.PAGOANTERIOR       = floatToXml(anteriorEfectivo);
        this.PAGOANTERIORCHEQUE = floatToXml(anteriorTalon);
        this.OBSERVACIONES      = observaciones;
        pendiente = true;
    }

    /**
     * Establece la factura como firmada. Campos a "0" (excepto pagos anteriores) y ESTATUS a "ENTREGADO".
     *
     * @param anteriorEfectivo Valor anterior efectivo.
     * @param anteriorTalon    Valor anterior talón.
     * @param observaciones    Observaciones.
     */
    public void setFacturaFirmada(float anteriorEfectivo, float anteriorTalon, String observaciones) {
        this.ESTATUS            = "ENTREGADO";
        this.TOTALEFECTIVO      = "0";
        this.TOTALCHEQUE        = "0";
        this.PAGOPARCIAL        = "0";
        this.PAGOPARCIALCHEQUE  = "0";
        this.PAGOANTERIOR       = floatToXml(anteriorEfectivo);
        this.PAGOANTERIORCHEQUE = floatToXml(anteriorTalon);
        this.OBSERVACIONES      = observaciones;
        pendiente = true;
    }

    /**
     * Establece la factura como entregada/enviada. ESTATUS a "ENTREGADO".
     *
     * @param efectivo         Valor efectivo.
     * @param talon            Valor efectivo.
     * @param anteriorEfectivo Valor anterior efectivo.
     * @param anteriorTalon    Valor anterior talón.
     * @param observaciones    Observaciones.
     */
    public void setFacturaEntregada(float efectivo, float talon, float anteriorEfectivo, float anteriorTalon, String observaciones) {
        this.ESTATUS = "ENTREGADO";
        float importe = Float.valueOf(this.IMPORTE.replace(",", "."));

        // PAGO PARCIAL
        if((efectivo + talon) < importe) {
            this.TOTALEFECTIVO = "0";
            this.TOTALCHEQUE = "0";
            this.PAGOPARCIAL = floatToXml(efectivo);
            this.PAGOPARCIALCHEQUE = floatToXml(talon);
        }

        // PAGO TOTAL
        else {
            this.TOTALEFECTIVO      = floatToXml(efectivo);
            this.TOTALCHEQUE        = floatToXml(talon);
            this.PAGOPARCIAL        = "0";
            this.PAGOPARCIALCHEQUE  = "0";
        }

        // PAGO ANTERIOR
        this.PAGOANTERIOR       = floatToXml(anteriorEfectivo);
        this.PAGOANTERIORCHEQUE = floatToXml(anteriorTalon);

        // OBSERVACIONES
        this.OBSERVACIONES      = observaciones;
        pendiente = true;
    }

    /**
     * Convierte un número decimal (tipo float) en un String. Si el número es entero, oculta el ".0".
     *
     * @param value Número a convertir.
     * @return      Cadena con el número pasado.
     */
    private String floatToXml(float value) {
        if((value %1) == 0)
            return String.valueOf(value).substring(0, String.valueOf(value).length()-2);
        else
            return String.valueOf(value);
    }
}
