package com.ricopia.goyanativoapp.obj;

/**
 * Clase estática con constantes y funciones operacionales estándar.
 */
public class G {
    public final static int ACTIVITY_ENVIARFACTURA = 1;
    public final static int UPDATE_FROM_NET = 1;
    public final static int UPDATE_FROM_FILE = 2;

    public final static int RUTAS_NORMALES = 0;
    public final static int RUTAS_INFORME  = 1;
    public final static String FILE_PATH_CARGAR  = "rutas.bin";
    public final static String FILE_PATH_INFORME = "informes.bin";
    public final static String OP_ACCION_CARGAR = "CargarRutas";
    public final static String OP_ACCION_INFORME = "Informe";

    /**
     * Realiza suma o resta con precisión correcta hasta 2 decimales.
     *
     * @param n1  Primer número operador.
     * @param n2  Segundo número operador.
     * @param sum Define si la operación es una suma (true) o una resta (false).
     * @return    Resultado.
     */
    public static float oper(float n1, float n2, boolean sum) {
        n1 *= 100;
        n2 *= 100;
        float res = (sum) ? n1+n2 : n1-n2;
        return res / 100;
    }

    /**
     * Convierte una cadena con formato número decimal en float (Java).
     *
     * @param number Cadena con número decimal a convertir.
     * @return       Número decimal en dato tipo float (Java).
     */
    public static float getFloatFromString(String number) {
        return Float.valueOf(number.replace(",", "."));
    }
}
