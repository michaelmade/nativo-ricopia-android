package com.ricopia.goyanativoapp.obj;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Objeto que almacena los datos de una ruta.
 *   - ID
 *   - IDRUTA
 *   - FECHAREPARTO
 *   - FACTURAS (+)
 */
public class Ruta implements Serializable {

    public String ID;
    public String IDRUTA;
    public String FECHAREPARTO;
    public ArrayList<Factura> FACTURAS;

    /**
     * Único constructor, inicializa el ArrayList de facturas.
     */
    public Ruta() {
        this.FACTURAS = new ArrayList<>();
    }

    /**
     * Devuelve el número de facturas.
     *
     * @return Número de facturas.
     */
    public int getSizeFacturas() {
        return this.FACTURAS.size();
    }

    /**
     * Devuelve la factura actual en cadena con formato XML.
     *
     * @return Cadena con formato XML de la factura.
     */
    private String toXML() {
        String rutaXml = "";

        // Comprobamos si hay alguna factura para entregar (distinta de abierto).
        boolean entregar = false;
        for(int i=0; i<getSizeFacturas(); i++) {
            if(!FACTURAS.get(i).ESTATUS.equals("ABIERTO")) {
                entregar = true;
                break;
            }
        }

        // Si se encontró alguna factura pendiente recorremos las facturas y las añadimos al XML.
        if(entregar) {
            Factura f;
            for(int i=0; i<getSizeFacturas(); i++) {
                f = FACTURAS.get(i);
                if(!f.ESTATUS.equals("ABIERTO")) { // Incluimos únicamente las facturas que hayan sido procesadas/enviadas.
                    rutaXml += f.toXML();
                }
            }

            rutaXml =
                    "<RUTA>"+
                        "<ID>"+ID+"</ID>"+
                        "<IDRUTA>"+IDRUTA+"</IDRUTA>"+
                        "<FECHAREPARTO>"+FECHAREPARTO+"</FECHAREPARTO>"+
                        "<FACTURAS>"+
                            rutaXml+
                        "</FACTURAS>"+
                    "</RUTA>";
        }

        return rutaXml;
    }

    /**
     * Devuelve una cadena con formato XML con todas las rutas que contienen facturas procesadas/enviadas (!= ABIERTO).
     *
     * @param rutas Lista de rutas a escanear
     * @return      Cadena con formato XML con las rutas
     */
    public static String toXML(ArrayList<Ruta> rutas) {
        String rutasXml = "";
        for(int i=0; i<rutas.size(); i++)
            rutasXml += rutas.get(i).toXML();
        return rutasXml;
    }
}
