package com.ricopia.goyanativoapp;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.ricopia.goyanativoapp.obj.Factura;
import com.ricopia.goyanativoapp.obj.G;
import com.ricopia.goyanativoapp.obj.Ruta;
import com.ricopia.goyanativoapp.task.EnviarRutasTask;
import com.ricopia.goyanativoapp.ui.dialog.DialogEnviarFactura;
import com.ricopia.goyanativoapp.ui.dialog.DialogInfo;
import com.ricopia.goyanativoapp.ui.dialog.DialogNuloFactura;
import com.ricopia.goyanativoapp.ui.dialog.DialogSalirFactura;

import java.util.ArrayList;


/**
 * Actividad mediante la cual se rellenarán los datos de las facturas para ser enviadas
 */
public class EnviarFacturaActivity extends AppCompatActivity {

    // VISTAS
    private ScrollView scvEnviarFactura;
    private LinearLayout prb;
    private TextView txvPagoAnterior;
    private EditText edtPagoAnteriorEfectivo1;
    private EditText edtPagoAnteriorEfectivo2;
    private EditText edtPagoAnteriorTalon1;
    private EditText edtPagoAnteriorTalon2;
    private TextView txvCobrado;
    private EditText edtCobradoEfectivo1;
    private EditText edtCobradoEfectivo2;
    private EditText edtCobradoTalon1;
    private EditText edtCobradoTalon2;
    private TextView txvAbono;
    private EditText edtNumeroAbono;
    private EditText edtAbono1;
    private EditText edtAbono2;
    private EditText edtObservaciones;
    private EditText edtObservacionesAbono;
    private CheckBox chkFirmado;
    private Button btnEnviar;
    private Button btnNulo;
    private FragmentManager fragmentManager;
    private Valores val;
    private Valores valBackup;

    // VARIABLES GLOBALES EN LA ACTIVIDAD
    private ArrayList<Ruta> rutas;
    private int nRuta;
    private int nFactura;
    private boolean nueva; // Ruta nueva (true) o ruta ya enviada y abierta desde informe (false)
    private Factura factura;

    /**
     * Objeto que mantendrá consistentes los datos de la factura para operar con ellos antes de enviarla
     */
    private class Valores {
        public float importe;
        public float cobrado;
        public float efectivo;
        public float talon;
        public float anteriorEfectivo;
        public float anteriorTalon;
        public float abono;
        public String nAbono;

        public Valores(float importe) {
            this.importe = importe;
            this.cobrado = 0;
            this.efectivo = 0;
            this.talon = 0;
            this.abono = 0;
            this.nAbono = "";
        }

        public void set(Valores valores) {
            this.cobrado = valores.cobrado;
            this.efectivo = valores.efectivo;
            this.talon = valores.talon;
            this.anteriorEfectivo = valores.anteriorEfectivo;
            this.anteriorTalon = valores.anteriorTalon;
            this.abono = valores.abono;
            this.nAbono = valores.nAbono;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_factura);

        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        try {
            rutas = (ArrayList<Ruta>) getIntent().getExtras().get("rutas");
            nRuta = getIntent().getExtras().getInt("nRuta");                // Número de ruta a la que pertenece la factura a cargar
            nFactura = getIntent().getExtras().getInt("nFactura");          // Número de factura a cargar
            nueva = getIntent().getExtras().getBoolean("nueva");
            factura = rutas.get(nRuta).FACTURAS.get(nFactura);
        } catch (NullPointerException ex) {
            factura = null;
        }

        if(factura != null) { // Si la factura existe
            fragmentManager = getSupportFragmentManager(); // Manejador de fragmentos para los diversos diálogos

            // Al pulsar una tecla...
            final View.OnKeyListener teclaPulsada = new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    valBackup.set(val);
                    val.cobrado = Float.valueOf(txvCobrado.getText().toString().replace(",", "."));
                    val.efectivo = getJoinNumber(edtCobradoEfectivo1, edtCobradoEfectivo2);
                    val.talon = getJoinNumber(edtCobradoTalon1, edtCobradoTalon2);
                    val.abono = getJoinNumber(edtAbono1, edtAbono2);
                    val.anteriorEfectivo = getJoinNumber(edtPagoAnteriorEfectivo1, edtPagoAnteriorEfectivo2);
                    val.anteriorTalon = getJoinNumber(edtPagoAnteriorTalon1, edtPagoAnteriorTalon2);

                    if(event.getAction() == KeyEvent.ACTION_UP)
                        actualizarCampos(v, true);

                    return false;
                }
            };

            // Al cambiar el foco...
            OnFocusChangeListener foco = new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus)
                        actualizarCampos(null, false);
                    /*else {
                        try {
                            String value = ((EditText) v).getText().toString();
                            if(value.equals("0") || value.equals("00")) {
                                //((EditText) v).setText("");
                                ((EditText) v).setSelection(0, 2);
                                //Toast.makeText(getApplicationContext(), "Start: " + ((EditText) v).getSelectionStart(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {}
                    }*/
                }
            };

            toolbar.setTitle(factura.NUMERO + " (" + factura.IMPORTE + "€)");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new OnClickListener() {
                // Si el usuario pulsa la flecha atrás, mostramos diálogo de confirmación para evitar perder datos.
                @Override
                public void onClick(View v) {
                    new DialogSalirFactura().show(fragmentManager, "SalirFactura");
                }
            });


            // CARGAMOS LAS VISTAS EN LAS VARIABLES PARA SU POSTERIOR ACCESO
            scvEnviarFactura = (ScrollView) findViewById(R.id.scvEnviarFactura);
            prb = (LinearLayout) findViewById(R.id.prbCargarRutas);
            btnEnviar = (Button) findViewById(R.id.btnEnviar);
            txvPagoAnterior = (TextView) findViewById(R.id.txvPagoAnterior);
            edtPagoAnteriorEfectivo1 = (EditText) findViewById(R.id.edtPagoAnteriorEfectivo1);
            edtPagoAnteriorEfectivo2 = (EditText) findViewById(R.id.edtPagoAnteriorEfectivo2);
            edtPagoAnteriorTalon1 = (EditText) findViewById(R.id.edtPagoAnteriorTalon1);
            edtPagoAnteriorTalon2 = (EditText) findViewById(R.id.edtPagoAnteriorTalon2);
            txvCobrado = (TextView) findViewById(R.id.txvCobrado);
            edtCobradoEfectivo1 = (EditText) findViewById(R.id.edtCobradoEfectivo1);
            edtCobradoEfectivo2 = (EditText) findViewById(R.id.edtCobradoEfectivo2);
            edtCobradoTalon1 = (EditText) findViewById(R.id.edtCobradoTalon1);
            edtCobradoTalon2 = (EditText) findViewById(R.id.edtCobradoTalon2);
            txvAbono = (TextView) findViewById(R.id.txvAbono);
            edtNumeroAbono = (EditText) findViewById(R.id.edtNumeroAbono);
            edtAbono1 = (EditText) findViewById(R.id.edtAbono1);
            edtAbono2 = (EditText) findViewById(R.id.edtAbono2);
            chkFirmado = (CheckBox) findViewById(R.id.chkFirmado);
            edtObservaciones = (EditText) findViewById(R.id.edtObservaciones);
            edtObservacionesAbono = (EditText) findViewById(R.id.edtObservacionesAbono);


            // PONEMOS LOS VALORES CARGADOS EN LOS CAMPOS
            // Importe
            val = new Valores(Float.valueOf(factura.IMPORTE.replace(",", ".")));
            // Pago anterior efectivo
            val.anteriorEfectivo = Float.valueOf(factura.PAGOANTERIOR.replace(",", "."));
            // Pago anterior talón
            val.anteriorTalon = Float.valueOf(factura.PAGOANTERIORCHEQUE.replace(",", "."));

            // Efectivo, cobrado y talón
            if(nueva) // Si la factura es nueva, el valor efectivo se calcula automáticamente para facilitar la operación al usuario.
                val.efectivo = val.cobrado = val.importe;
            else { // Si la factura proviene del informe...
                if(factura.ESTATUS.equals("NULO"))
                    val.efectivo = val.cobrado = 0; // Si es nulo, el efectivo se pone a 0
                else {
                    val.efectivo = Float.valueOf(factura.PAGOPARCIAL.replace(",", "."));
                    val.efectivo += Float.valueOf(factura.TOTALEFECTIVO.replace(",", "."));
                    val.talon = Float.valueOf(factura.PAGOPARCIALCHEQUE.replace(",", "."));
                    val.talon += Float.valueOf(factura.TOTALCHEQUE.replace(",", "."));
                    val.cobrado = G.oper(val.efectivo, val.talon, true);
                    if(factura.ESTATUS.equals("FIRMA")) { // Si es firmado, marcamos el checkbox de firma y bloqueamos los campos (pago anterior no incluido).
                        chkFirmado.setChecked(true);
                        habilitarCamposFirma(false);
                    }
                }
            }

            // Observaciones
            edtObservaciones.setText(factura.OBSERVACIONES);

            // Salvamos valores actuales. Después el usuario puede modificarlos y ser incorrectos,
            // necesitamos este backup para restablecer los valores después de notificar al usuario.
            valBackup = new Valores(Float.valueOf(factura.IMPORTE.replace(",", ".")));
            valBackup.set(val);

            // Actualizamos la interfaz con los valores cargados
            actualizarCampos(null, false);


            // LISTENERS PARA LOS CAMPOS EDITABLES
            edtPagoAnteriorEfectivo1.setOnKeyListener(teclaPulsada);
            edtPagoAnteriorEfectivo2.setOnKeyListener(teclaPulsada);
            edtPagoAnteriorTalon1.setOnKeyListener(teclaPulsada);
            edtPagoAnteriorTalon2.setOnKeyListener(teclaPulsada);
            edtCobradoEfectivo1.setOnKeyListener(teclaPulsada);
            edtCobradoEfectivo2.setOnKeyListener(teclaPulsada);
            edtCobradoTalon1.setOnKeyListener(teclaPulsada);
            edtCobradoTalon2.setOnKeyListener(teclaPulsada);
            edtNumeroAbono.setOnKeyListener(teclaPulsada);
            edtAbono1.setOnKeyListener(teclaPulsada);
            edtAbono2.setOnKeyListener(teclaPulsada);
            edtPagoAnteriorEfectivo1.setOnFocusChangeListener(foco);
            edtPagoAnteriorEfectivo2.setOnFocusChangeListener(foco);
            edtPagoAnteriorTalon1.setOnFocusChangeListener(foco);
            edtPagoAnteriorTalon2.setOnFocusChangeListener(foco);
            edtCobradoEfectivo1.setOnFocusChangeListener(foco);
            edtCobradoEfectivo2.setOnFocusChangeListener(foco);
            edtCobradoTalon1.setOnFocusChangeListener(foco);
            edtCobradoTalon2.setOnFocusChangeListener(foco);
            edtNumeroAbono.setOnFocusChangeListener(foco);
            edtAbono1.setOnFocusChangeListener(foco);
            edtAbono2.setOnFocusChangeListener(foco);

            // CHECKBOX FIRMADO (MARCAR PARA FIRMAR)
            chkFirmado.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        // Hacemos un backup de los datos actuales (antes de borrarlos para desactivar las cajas)
                        val.anteriorEfectivo = getJoinNumber(edtPagoAnteriorEfectivo1, edtPagoAnteriorEfectivo2);
                        val.anteriorTalon = getJoinNumber(edtPagoAnteriorTalon1, edtPagoAnteriorTalon2);
                        val.nAbono = edtNumeroAbono.getText().toString();
                        edtNumeroAbono.setText(""); // Borramos el número de abono
                        valBackup.set(val); // BACKUP

                        val = new Valores(val.importe);
                        // El pago anterior no cambia, por lo que recargamos
                        val.anteriorEfectivo = valBackup.anteriorEfectivo;
                        val.anteriorTalon = valBackup.anteriorTalon;

                        habilitarCamposFirma(false); // Deshabilitamos los campos que no se pueden editar al firmar la factura
                        actualizarCampos(null, false);
                    } else {
                        val.set(valBackup); // Cargamos los datos almacenados antes de marcar el checkbox de firma
                        habilitarCamposFirma(true); // Volvemos a habilitar los campos de los importes
                        edtNumeroAbono.setText(val.nAbono); // Cargamos el número de abono

                        // Actualizamos la interfaz con los valores cargados
                        actualizarCampos(null, false);
                    }
                    edtObservaciones.requestFocus();
                }
            });

            // BOTON ENVIAR FACTURA
            btnEnviar = (Button) findViewById(R.id.btnEnviar);
            btnEnviar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogEnviarFactura dialogEnviarFactura = new DialogEnviarFactura();

                    Bundle arguments = new Bundle();
                    arguments.putString("factura", factura.NUMERO);
                    // Enviamos resumen de la factura al diálogo para que lo vea el usuario
                    arguments.putString("datos",
                                    "  Pago anterior: " + (val.anteriorEfectivo+val.anteriorTalon) + " €\n" +
                                    "  Valor cobrado: " + val.cobrado + " €\n" +
                                    "  Valor abono: " + val.abono + " €\n" +
                                    "  IMPORTE factura: " + val.importe + " €");
                    dialogEnviarFactura.setArguments(arguments);

                    // Mostramos diálogo para confirmar que el usuario desea enviar la factura con los valores actuales
                    dialogEnviarFactura.show(fragmentManager, "EnviarFactura");
                }
            });

            // BOTON ENVIAR FACTURA NULA
            btnNulo = (Button) findViewById(R.id.btnNulo);
            btnNulo.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Mostramos diálogo para confirmar que el usuario desea enviar la factura como nula
                    new DialogNuloFactura().show(fragmentManager, "NuloFactura");
                }
            });

            // Hacemos scroll hacia arriba y eliminamos focos de EditText
            scvEnviarFactura.post(new Runnable() {
                @Override
                public void run() {
                    scvEnviarFactura.fullScroll(ScrollView.FOCUS_UP);
                    scvEnviarFactura.requestFocus();
                }
            });
        }
        else { // Si no se ha podido cargar la factura, cerramos la actividad y notificamos al usuario
            finish();
            Toast.makeText(getApplicationContext(), "ERROR: No se han podido recuperar los datos de la factura", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Si el usuario pulsa la tecla atrás, mostramos diálogo de confirmación para evitar perder datos
        if (keyCode == KeyEvent.KEYCODE_BACK)
            new DialogSalirFactura().show(fragmentManager, "SalirFactura");
        return true;
    }

    /**
     * Bloquea o habilita los campos de cobrado y abono.
     *
     * @param bloq Habilita (true) deshabilita (false).
     */
    private void habilitarCamposFirma(boolean bloq) {
        edtCobradoEfectivo1.setEnabled(bloq);
        edtCobradoEfectivo2.setEnabled(bloq);
        edtCobradoTalon1.setEnabled(bloq);
        edtCobradoTalon2.setEnabled(bloq);
        edtNumeroAbono.setEnabled(bloq);
        edtAbono1.setEnabled(bloq);
        edtAbono2.setEnabled(bloq);
    }

    /**
     * Extra la parte entera o decimal de un número almacenado en una cadena.
     *
     * @param parteEntera Parte a extraer. True: entera, False: decimal.
     * @param n           Número del cual extraeremos la parte entera o decimal (en cadena).
     * @param delimiter   Delimitador que separa la parte entera de la decimal.
     * @return            Parte decimal o entera del número pasado.
     */
    private String getSeparatedNumber(boolean parteEntera, String n, String delimiter) {
        int pos = n.indexOf(delimiter);
        if(pos == -1) // Si no encuentra el delimitador quiere decir que no hay decimales, devolvemos 0 o 00.
            return parteEntera ? (n.length() < 2) ? n+"0" : n : "00";
        else {
            if(parteEntera)
                return n.substring(0, pos);
            else {
                String str = n.substring(pos+1, n.length());
                return (str.length() < 2) ? str+"0" : str;
            }
        }
    }

    /**
     * Devuelve un número tipo float a partir de 2 campos de texto (uno con parte entera y otro con parte decimal).
     *
     * @param parteEntera  EditText con la parte entera del número a generar.
     * @param parteDecimal EditText con la parte decimal del número a generar.
     * @return             Número tipo float.
     */
    private float getJoinNumber(EditText parteEntera, EditText parteDecimal) {
        float total = 0;
        try { total += Float.valueOf(parteEntera.getText().toString());         } catch (Exception ex) {}
        try { total += Float.valueOf("0."+(parteDecimal.getText().toString())); } catch (Exception ex) {}
        return total;
    }

    /**
     * Extrae los datos del formulario, prepara y envía la factura al servidor.
     *
     * @param isNula True si la factura será enviada como nula, False para cualquier otro tipo de factura.
     */
    public void enviarFactura(boolean isNula) {
        // Nula
        if(isNula)
            rutas.get(nRuta).FACTURAS.get(nFactura).setFacturaNula(val.anteriorEfectivo, val.anteriorTalon, edtObservaciones.getText().toString());
        // Firmada
        else if(chkFirmado.isChecked())
            rutas.get(nRuta).FACTURAS.get(nFactura).setFacturaFirmada(val.anteriorEfectivo, val.anteriorTalon, edtObservaciones.getText().toString());
        // Cualquier otro caso
        else {
            String observaciones = edtObservaciones.getText().toString();
            if(val.abono > 0)
                observaciones = edtObservacionesAbono.getText().toString() + observaciones;
            rutas.get(nRuta).FACTURAS.get(nFactura).setFacturaEntregada(val.efectivo, val.talon, val.anteriorEfectivo, val.anteriorTalon, observaciones);
        }

        EnviarRutasTask enviarRutasTask = new EnviarRutasTask(getApplicationContext(), this, (nueva) ? G.OP_ACCION_CARGAR : G.OP_ACCION_INFORME);
        enviarRutasTask.setPrbCargarRutas(prb, scvEnviarFactura); // Pasamos las Views del progressBar (de carga) y lista (para ocultar).
        enviarRutasTask.updateToNet(rutas);
    }

    /**
     * Función para actualizar los valores de los campos, completa con 0 ó 00 si el usuario deja un campo vacío.
     *
     * @param v      Último campo que el usuario a modificado, se pasa este parámetro para no recargar este campo y realizar los cálculos necesarios con el nuevo valor.
     * @param modify Si ha habido (true) o no (false) cambios en los campos.
     */
    private void actualizarCampos(View v, boolean modify) {
        if(modify) {
            float cobrado = G.oper(val.importe, val.abono, false); // val.importe - val.abono
            // Efectivo (cobrado)
            if((v == edtCobradoEfectivo1) || (v == edtCobradoEfectivo2)) {
                if(G.oper(val.efectivo, val.talon, true) > cobrado) { // (val.efectivo + val.talon) > val.cobrado
                    DialogInfo d = new DialogInfo();
                    float total = G.oper(val.abono, G.oper(val.efectivo, val.talon, true), true); // val.abono + val.efectivo + val.talon
                    d.set("Valor efectivo incorrecto", "La suma del efectivo, talón y abono ("+total+" €) no puede ser superior al importe de la factura.\n\n-> Efectivo: " + val.efectivo + " €\n    Talón: " + val.talon + " €\n    Abono: " + val.abono + " €\n    IMPORTE: " + val.importe + " €");
                    d.show(fragmentManager, "valorEfectivoSuperior");
                    val.set(valBackup);
                    actualizarCampos(null, false);
                    ((EditText) v).setSelection(((EditText) v).getText().length());
                    return;
                }
                else {
                    val.cobrado = G.oper(val.efectivo, val.talon, true); // val.efectivo + val.talon
                }
            }
            // Talón (cobrado)
            else if((v == edtCobradoTalon1) || (v == edtCobradoTalon2)) {
                if((val.cobrado - val.talon) >= 0)
                    val.efectivo = G.oper(val.cobrado, val.talon, false); // val.cobrado - val.talon
                else {
                    DialogInfo d = new DialogInfo();
                    d.set("Valor talón incorrecto", "La suma del efectivo y el talón no puede ser superior al valor cobrado.\n\n    Efectivo: " + val.efectivo + " €\n-> Talón: " + val.talon + " €\n    Cobrado: " + val.cobrado + " €");
                    d.show(fragmentManager, "valorTalonSuperior");
                    val.set(valBackup);
                    actualizarCampos(null, false);
                    ((EditText) v).setSelection(((EditText) v).getText().length());
                    return;
                }
            }
            // Abono (valor)
            else if((v == edtAbono1) || (v == edtAbono2)) {
                float total = G.oper(val.cobrado, val.abono, true);

                if(G.oper(val.importe, total, false) < 0) {
                    DialogInfo d = new DialogInfo();
                    d.set("Valor de abono incorrecto", "La suma del abono y valor cobrado no puede ser superior al importe de la factura.\n\n    Cobrado: " + val.cobrado + " €\n-> Abono: " + val.abono + " €\n    IMPORTE: " + val.importe + " €");
                    d.show(fragmentManager, "valorAbonoSuperior");
                    val.set(valBackup);
                    actualizarCampos(null, false);
                    ((EditText) v).setSelection(((EditText) v).getText().length());
                    return;
                }
            }
        }

        // COBRADO
        txvCobrado.setText(getSeparatedNumber(true, Float.toString(val.cobrado), ".") + "," + getSeparatedNumber(false, Float.toString(val.cobrado), "."));
        // EFECTIVO
        if(v != edtCobradoEfectivo1) {
            if(val.efectivo < 1)
                edtCobradoEfectivo1.setText("0");
            else
                edtCobradoEfectivo1.setText(getSeparatedNumber(true, Float.toString(val.efectivo), "."));
        }
        if(v != edtCobradoEfectivo2) {
            if((val.efectivo %1) == 0)
                edtCobradoEfectivo2.setText("00");
            else
                edtCobradoEfectivo2.setText(getSeparatedNumber(false, Float.toString(val.efectivo), "."));
        }
        // TALON
        if(v != edtCobradoTalon1) {
            if(val.talon < 1)
                edtCobradoTalon1.setText("0");
            else
                edtCobradoTalon1.setText(getSeparatedNumber(true, Float.toString(val.talon), "."));
        }
        if(v != edtCobradoTalon2) {
            if((val.talon %1) == 0)
                edtCobradoTalon2.setText("00");
            else
                edtCobradoTalon2.setText(getSeparatedNumber(false, Float.toString(val.talon), "."));
        }

        // ABONO
        txvAbono.setText(getSeparatedNumber(true, Float.toString(val.abono), ".") + "," + getSeparatedNumber(false, Float.toString(val.abono), "."));
        if(v != edtAbono1) {
            if(val.abono < 1)
                edtAbono1.setText("0");
            else
                edtAbono1.setText(getSeparatedNumber(true, Float.toString(val.abono), "."));
        }
        if(v != edtAbono2) {
            if((val.abono %1) == 0)
                edtAbono2.setText("00");
            else
                edtAbono2.setText(getSeparatedNumber(false, Float.toString(val.abono), "."));
        }

        // OBSERVACIONES ABONO
        if(val.abono > 0) {
            edtObservacionesAbono.setText("[Abono: "+edtNumeroAbono.getText()+" - "+val.abono+" €] ");
        }
        else
            edtObservacionesAbono.setText("");

        // PAGO ANTERIOR
        txvPagoAnterior.setText(getSeparatedNumber(true, Float.toString(val.anteriorEfectivo + val.anteriorTalon), ".") + "," + getSeparatedNumber(false, Float.toString(val.anteriorEfectivo+val.anteriorTalon), "."));
        // EFECTIVO
        if(v != edtPagoAnteriorEfectivo1) {
            if(val.anteriorEfectivo < 1)
                edtPagoAnteriorEfectivo1.setText("0");
            else
                edtPagoAnteriorEfectivo1.setText(getSeparatedNumber(true, Float.toString(val.anteriorEfectivo), "."));
        }
        if(v != edtPagoAnteriorEfectivo2) {
            if((val.anteriorEfectivo %1) == 0)
                edtPagoAnteriorEfectivo2.setText("00");
            else
                edtPagoAnteriorEfectivo2.setText(getSeparatedNumber(false, Float.toString(val.anteriorEfectivo), "."));
        }
        // TALON
        if(v != edtPagoAnteriorTalon1) {
            if(val.anteriorTalon < 1)
                edtPagoAnteriorTalon1.setText("0");
            else
                edtPagoAnteriorTalon1.setText(getSeparatedNumber(true, Float.toString(val.anteriorTalon), "."));
        }
        if(v != edtPagoAnteriorTalon2) {
            if((val.anteriorTalon %1) == 0)
                edtPagoAnteriorTalon2.setText("00");
            else
                edtPagoAnteriorTalon2.setText(getSeparatedNumber(false, Float.toString(val.anteriorTalon), "."));
        }
    }
}
